#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
This script executes infra tests over a given host using either ansible
backend or localhost.
'''

import sys
import os
import traceback

from testinfra import get_host
from unittest import TextTestRunner, makeSuite, TestSuite
from xmlrunner import XMLTestRunner

from defs.functions import red,yellow,white,resultTable,initTable
from defs.testLoader import getTest
from constants import constants as c

class RunTestInfra(object):
    def __init__(self):
        self.wasSuccessful = False
        self.output = initTable()
        self.host = self.getHost()

    def getHost(self):
        '''
        Host connection
        '''
        if c.remote:
            if os.path.isfile(c.inventoryPath):
                host = get_host('ansible://%s?ansible_inventory=%s' %
                (c.inventoryHost, c.inventoryPath) )
            else:
                print (red("No inventory present:\n%s" % c.inventoryPath))
                assert False
                sys.exit(-1)
        else:
            host = get_host('local://')
        try:
            host.file('/')
        except Exception as err:
            print (err)
            print (red("\n-- ERROR: Sin conexión al server\n"))
            assert False
            sys.exit(-1)
        return host

    def setUpTest(self, testClass):
        '''
        Setup host
        '''
        testClass.host = self.host
        testClass.output = self.output
        return testClass

    def runTests(self, testName):
        '''
        Run test suite from testCases script
        '''
        myTestCasesDict = getTest(testName)
        for testClass in myTestCasesDict['testCases']:
            currentTest = self.setUpTest(testClass)
            print (currentTest)
            ts = TestSuite(makeSuite(currentTest, 'test'))
            result = XMLTestRunner(output='%s' % c.xmlPathToSave).run(ts)
            self.wasSuccessful = True if result.wasSuccessful() else False

    def printOutput(self):
        resultTable(self.output)

    def geTestInfo(self):
        print('\n[+] %s' % yellow('Initializing program'))
        print('-----------------------------------------')
        print('Host: %s' % white(c.inventoryHost))
        print('Project: %s' % white(c.project))
        print('Env: %s' % white(c.env))
        print('-----------------------------------------\n')

if __name__ == '__main__':
    app = RunTestInfra()
    app.geTestInfo()
    casesList = os.getenv('tests', 'general,system').split(',')
    try:
        for i in casesList:
            app.runTests(i)
    except Exception as err:
        traceback.print_exc()
    finally:
        app.printOutput()
        print(app.wasSuccessful)
        if app.wasSuccessful is 'False' or app.wasSuccessful is False:
            assert False
