rsyslog-service
=========

Tasks for setting up rsyslog client.

Requirements
------------

EL based system.

Role Variables
--------------

None.

Dependencies
 -----------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - rsyslog-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
