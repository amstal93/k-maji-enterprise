Proxy-service
=========

Tasks for setting up Squid proxy service.

Requirements
------------

EL based system.

Role Variables
--------------

None.

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - firewalld-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: luis.carrillo@kronops.com.mx.
