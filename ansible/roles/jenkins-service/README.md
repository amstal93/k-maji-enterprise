jenkins-service
===============

Tasks for setting up jenkins service.

Requirements
------------

EL based system.

Role Variables
--------------

jenkins_http_port: 8080
jenkins_url_prefix: ""
jenkins_home: /var/lib/jenkins
jenkins_hostname: localhost
jenkins_admin_username: admin
jenkins_admin_password: admin
jenkins_jobs_path: /etc/ansible/roles/jenkins-service/files/var/lib/jenkins/jobs
jenkins_plugins:
  - 'ansible'
  - 'ansicolor'
  - 'greenballs'
  - 'timestamper'
  - 'ws-cleanup'
  - 'ldap'
  - 'github'
  - 'dashboard-view'
  - 'nested-view'
  - 'slack'
  - 'pipeline-build-step'
  - 'pipeline-graph-analysis'
  - 'pipeline-input-step'
  - 'pipeline-milestone-step'
  - 'pipeline-model-api'
  - 'pipeline-model-declarative-agent'
  - 'pipeline-model-definition'
  - 'pipeline-rest-api'
  - 'pipeline-stage-step'
  - 'pipeline-stage-tags-metadata'
  - 'pipeline-stage-view'
  - 'shiningpanda'
  - 'xvfb'
  - 'test-results-analyzer'
  - 'simple-theme-plugin'
  - 'rebuild'
  - 'image-gallery'
  - 'copyartifact'
  - 'swarm'
  - 'envinject'
  - 'blueocean'
jenkins_repo_path: /tmp/jenkins.io.key
jenkins_repo_key: https://pkg.jenkins.io/redhat/jenkins.io.key
# Itop vars
install_dir: web
use_https: N
itop_user: admin
itop_pass: admin

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - jenkins-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
